/* Generated automatically. */
static const char configuration_arguments[] = "/ssd/shunyen/sdk-4.9/gcc/configure --prefix=/tmp/asdk64-4.9.3-2180/release/asdk64-4.9.3/linux/eglibc --build=i686-pc-linux --host=i686-pc-linux --target=aarch64-linux-gnu --enable-languages=c,c++ --enable-threads --enable-shared --enable-lto --enable-multilib --enable-target-optspace --enable-__cxa_atexit --enable-linker-build-id --disable-sjlj-exceptions --disable-fixed-point --disable-decimal-float --enable-tls --disable-nls --disable-libssp --disable-libgomp --disable-libmudflap --disable-libquadmath --disable-libffi --disable-libitm --disable-libstdc__-v3 --disable-libsanitizer --with-arch=armv8-a --with-gnu-ld --with-gnu-as --with-dwarf2 --enable-symvers=gnu --with-sdk=asdk64 --with-sysroot='/tmp/asdk64-4.9.3-2180/release/asdk64-4.9.3/linux/eglibc' --with-pkgversion='Realtek ASDK64-4.9.3 Build 2180' --program-transform-name='s&^&aarch64-linux-&' --enable-multiarch";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "arch", "armv8-a" } };
